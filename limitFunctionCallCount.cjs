function limitFunctionCallCount(cb,n){
    try{
        let callCount = 0
        function limit(){
            if(callCount<n){
                callCount++
                cb()
            }else{
                return null
            }
        }
        return limit
    }catch(error){
        console.log('throw error')
    }
}
module.exports = limitFunctionCallCount
