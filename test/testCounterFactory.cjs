let counterFactory = require("../counterFactory.cjs")

let count = counterFactory()

console.log(count.increment())
console.log(count.increment())
console.log(count.decrement())
console.log(count.decrement())