let cachefunction = require("../cacheFunction.cjs")

function add(a,b){
    console.log("Adding",a,"and",b)
    return a+b
}

let cacheAdd = cachefunction(add)

console.log(cacheAdd(1,2))
console.log(cacheAdd(2,3))
console.log(cacheAdd(1,2))