let limitFunctionCallCount = require("../limitFunctionCallCount.cjs")

function myFunction(){
    console.log("Function called")
}

let limitedFunction =  limitFunctionCallCount(myFunction,4)

limitedFunction()
limitedFunction()
limitedFunction()
limitedFunction()

