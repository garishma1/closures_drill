function cacheFunction(cb){
    try{
        let cache = {}
        function cacheFun(...args){
            let key = JSON.stringify(args)
            if(!(key in cache)){
                cache[key]=cb(...args)
            }
            return cache[key]
        }
        return cacheFun
    }catch(error){
        console.log("throw error")
    }  
}
module.exports = cacheFunction